import { FC, useState } from 'react';
import { RiTodoLine } from 'react-icons/ri'
import './App.css';
import InputField from './components/InputField/InputField';
import { Todo } from './models/Todo';


const App: FC = () => {
  const [todo, setTodo] = useState<string>('');
  const [todos, setTodos] = useState<Todo[]>([]);
  const handleAdd = (e: React.FormEvent) => {
    e.preventDefault();
    if (todo) {
      setTodos([...todos, { id: Date.now(), todo, isDone: false }]);
      setTodo('');
    }
  }
  return (
    <div className="App" >
      <span className="heading">
        To-do List <RiTodoLine />
      </span>
      <InputField todo={todo} setTodo={setTodo} handleAdd={(e) => handleAdd(e)} />
    </div >
  )
}

export default App;
