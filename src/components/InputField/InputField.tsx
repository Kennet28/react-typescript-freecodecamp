import { FC, useRef } from 'react'
import './InputField.css'
import { InputFieldProps } from './InputField.props'
const InputField: FC<InputFieldProps> = ({ todo, setTodo, handleAdd }) => {
    const inputRef = useRef<HTMLInputElement>(null);
    return ((
        <form className="input" onSubmit={(e) => {
            handleAdd(e)
            inputRef.current?.blur()
        }}>
            <input
                ref={inputRef}
                value={todo}
                onChange={(e) => {
                    setTodo(e.target.value)
                }}
                type="input" placeholder='Enter a task' className='input__box' />
            <button className="input__submit" type="submit">Go</button>
        </form>
    ))
}

export default InputField