import { Dispatch, SetStateAction } from "react";

export interface InputFieldProps {
    todo: string;
    setTodo: Dispatch<SetStateAction<string>>;
    handleAdd: (e: FormEvent) => void;
}